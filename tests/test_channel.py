import asyncio
import platform
import ssl
import struct

import pytest

from aiomas.exceptions import RemoteException
import aiomas.channel
import aiomas.codecs
import aiomas.local_queue as lq
import aiomas.util


ON_WINDOWS = platform.system() == 'Windows'


class Context:
    def __init__(self, loop, addr):
        self.loop = loop
        self.addr = addr
        self.server = None

    async def connect(self, **kwargs):
        return await aiomas.channel.open_connection(self.addr, loop=self.loop,
                                                    **kwargs)

    async def start_server(self, handle_client, **kwargs):
        self.server = await aiomas.channel.start_server(
            self.addr, handle_client, loop=self.loop, **kwargs)

    async def start_server_and_connect(self, handle_client, server_kwargs=None,
                                       client_kwargs=None):
        if server_kwargs is None:
            server_kwargs = {}

        if client_kwargs is None:
            client_kwargs = {}

        await self.start_server(handle_client, **server_kwargs)
        return await self.connect(**client_kwargs)

    async def close_server(self):
        if self.server is not None:
            server, self.server = self.server, None
            server.close()
            await server.wait_closed()


@pytest.fixture(params=['tcp', 'unix', 'localqueue'])
def ctx(request, event_loop):
    """Generate tests with TCP sockets, Unix domain sockets and a local queue.
    """
    addr_type = request.param
    if addr_type == 'tcp':
        port = request.getfuncargvalue('unused_tcp_port')
        addr = ('127.0.0.1', port)
    elif addr_type == 'unix':
        if ON_WINDOWS:
            pytest.skip('Not implemented on Windows')
        tmpdir = request.getfuncargvalue('short_tmpdir')
        addr = tmpdir.join('sock').strpath
    elif addr_type == 'localqueue':
        addr = lq.get_queue('test')
    else:
        raise RuntimeError('Unknown addr type: %s' % addr_type)

    ctx = Context(event_loop, addr)

    yield ctx

    if addr_type == 'localqueue':
        lq.clear_queue_cache()
    # Shudown the server and wait for all pending tasks to finish:
    aiomas.run(ctx.close_server())
    aiomas.run(asyncio.gather(*asyncio.Task.all_tasks(event_loop),
                              return_exceptions=True))


@pytest.mark.asyncio
async def test_channel_request_reply(ctx):
    results = []

    async def handle_client(channel):
        request = await channel.recv()
        results.append(request.content)
        await request.reply('cya')
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    reply = await channel.send('ohai')
    results.append(reply)
    await channel.close()

    assert results == ['ohai', 'cya']


@pytest.mark.asyncio
async def test_channel_request_reply_with_default_loop(ctx):
    results = []

    async def handle_client(channel):
        request = await channel.recv()
        results.append(request.content)
        await request.reply('cya')
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    reply = await channel.send('ohai')
    results.append(reply)
    await channel.close()

    assert results == ['ohai', 'cya']


@pytest.mark.asyncio
async def test_channel_request_error(ctx):
    results = []

    async def handle_client(channel):
        request = await channel.recv()
        results.append(request.content)
        await request.fail(ValueError('cya'))
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    with pytest.raises(RemoteException) as exc_info:
        await channel.send('ohai')
    results.append(str(exc_info.value))
    await channel.close()

    if type(ctx.addr) is tuple:
        assert results == ['ohai', 'Origin: %r\n'
                                   'ValueError: cya\n' % (ctx.addr,)]
    elif isinstance(ctx.addr, lq.LocalQueue):
        assert results == ['ohai', "Origin: LocalQueue('test')\n"
                                   "ValueError: cya\n"]
    else:
        assert results[0] == 'ohai'
        assert results[1].endswith('sock\nValueError: cya\n')


@pytest.mark.asyncio
async def test_parallel_messages(ctx):
    """We should be able to send many messages in parallel before waiting for
    replies to come in."""
    async def handle_client(channel):
        request = await channel.recv()
        await request.reply('cya1')
        request = await channel.recv()
        await request.reply('cya2')
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    req1 = channel.send('ohai1')
    req2 = channel.send('ohai2')
    replies = await asyncio.gather(req1, req2, loop=ctx.loop)
    assert replies == ['cya1', 'cya2']
    await channel.close()


@pytest.mark.asyncio
async def test_incomplete_message(ctx):
    """The client calls recv() but only parts of the msg. are transmitted."""
    if isinstance(ctx.addr, lq.LocalQueue):
        pytest.skip('LocalQueue always sends complete messages.')

    async def handle_client(channel):
        channel.transport.write(aiomas.channel.Header.pack(4) + b'oh')
        channel.transport.write_eof()
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    with pytest.raises(ConnectionResetError):
        # Note: In previous versions, this was an IncompleteMessage error
        await channel.recv()
    await channel.close()


@pytest.mark.asyncio
async def test_incomplete_reply_for_sender(ctx):
    """The client calls send() and only parts of the reply are transmitted."""
    if isinstance(ctx.addr, lq.LocalQueue):
        pytest.skip('LocalQueue always sends complete messages.')

    async def handle_client(channel):
        await channel.recv()
        channel.transport.write(aiomas.channel.Header.pack(3) + b'cy')
        channel.transport.write_eof()
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    with pytest.raises(ConnectionResetError):
        # Note: In previous versions, this was an IncompleteMessage error
        await channel.send('ohai')
    await channel.close()


@pytest.mark.asyncio
async def test_eof_received(ctx):
    """EOF received while waiting for an incoming message."""
    if isinstance(ctx.addr, lq.LocalQueue):
        pytest.skip('LocalQueue does not support EOF.')

    async def handle_client(channel):
        channel.transport.write_eof()
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    with pytest.raises(ConnectionResetError):
        await channel.recv()
    await channel.close()


@pytest.mark.asyncio
async def test_eof_received_before_recv(ctx):
    """EOF received before we try to recv() a message."""
    if isinstance(ctx.addr, lq.LocalQueue):
        pytest.skip('LocalQueue does not support EOF.')

    async def handle_client(channel):
        channel.transport.write_eof()

    channel = await ctx.start_server_and_connect(handle_client)
    await asyncio.sleep(.01, loop=ctx.loop)
    assert type(channel._exception) is ConnectionResetError
    with pytest.raises(ConnectionError):
        await channel.recv()
    await channel.close()


@pytest.mark.asyncio
async def test_con_closed_before_send(ctx):
    """Connection is closed before we try to send."""
    async def handle_client(channel):
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    await asyncio.sleep(.1, loop=ctx.loop)
    assert type(channel._exception) is ConnectionResetError
    with pytest.raises(ConnectionError):
        await channel.send('ohai')
    await channel.close()


@pytest.mark.asyncio
async def test_get_extra_info(ctx):
    async def handle_client(channel):
        res = channel.get_extra_info('peername')
        if type(ctx.addr) is tuple:
            assert res[0] == ctx.addr[0]  # We don't care about the client port
        elif isinstance(ctx.addr, lq.LocalQueue):
            assert res == str(ctx.addr)
        else:
            assert not res
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    res = channel.get_extra_info('peername')
    if type(ctx.addr) is tuple:
        assert res == ctx.addr
    elif isinstance(ctx.addr, lq.LocalQueue):
        assert res == str(ctx.addr)
    else:
        assert res.endswith('/sock')
    await asyncio.sleep(0.001)
    await channel.close()


@pytest.mark.asyncio
async def test_competing_recv(ctx):
    """The client spawns two sub-coroutines which both try so receive from
    the same channel. One should succeed, one should fail."""
    async def handle_client(channel):
        await asyncio.sleep(0.1, loop=ctx.loop)
        await channel.send('ohai')
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    results = set()

    async def sub():
        try:
            msg = await channel.recv()
            results.add(True)
            await msg.reply('')
        except RuntimeError:
            results.add(False)

    await asyncio.gather(sub(), sub(), loop=ctx.loop)
    assert results == {True, False}

    await channel.close()


@pytest.mark.asyncio
async def test_too_long_messages(ctx, monkeypatch):
    monkeypatch.setattr(aiomas.channel, 'Header', struct.Struct('!B'))
    msg = 'a' * 256

    async def handle_client(channel):
        with pytest.raises(ConnectionError):
            await channel.recv()

        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    with pytest.raises(ValueError):
        await channel.send(msg)

    await channel.close()


@pytest.mark.asyncio
async def test_invalid_message_type(ctx):
    """The client spawns two sub-coroutines which both try so receive from
    the same channel. One should succeed, one should fail."""
    async def handle_client(channel):
        content = channel.codec.encode([4, 1, 'spam'])
        content = aiomas.channel.Header.pack(len(content)) + content
        channel.transport.write(content)
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)

    with pytest.raises(RuntimeError) as exc_info:
        await channel.recv()

    assert str(exc_info.value) == 'Invalid message type 4'
    await channel.close()


@pytest.mark.asyncio
async def test_invalid_address_type():
    with pytest.raises(ValueError) as e_info:
        await aiomas.channel.open_connection(42)
    assert str(e_info.value) == 'Unknown address type: 42'

    with pytest.raises(ValueError) as e_info:
        await aiomas.channel.start_server(42, None)
    assert str(e_info.value) == 'Unknown address type: 42'


@pytest.mark.asyncio
async def test_channel_flow_control(ctx, monkeypatch):
    if isinstance(ctx.addr, lq.LocalQueue):
        pytest.skip('LocalQueue does not implment FlowControl.')

    results = []

    def debug_mock(*args):
        results.append(args)

    monkeypatch.setattr(aiomas.channel.logger, 'debug', debug_mock)
    ctx.loop.set_debug(True)
    outmsg = 'ohai' * 1000
    n_msgs = 1000

    async def handle_client(channel):
        for _ in range(n_msgs):
            request = await channel.recv()
            await request.reply('cya')
        await channel.close()

    channel = await ctx.start_server_and_connect(handle_client)
    assert hasattr(channel, 'send')
    tasks = []
    for _ in range(n_msgs):
        tasks.append(channel.send(outmsg))
    await asyncio.gather(*tasks, loop=ctx.loop)
    await channel.close()

    assert results
    assert len(results) % 2 == 0  # 1 message for pausing, 1 for resuming


@pytest.mark.asyncio
async def test_ssl(ctx, certs):
    if type(ctx.addr) is not tuple:
        pytest.skip('Test SSL only with TCP sockets.')

    async def handle_client(channel):
        request = await channel.recv()
        assert request.content == 'ohai'
        await request.reply('cya')
        await channel.close()

    server_ctx = aiomas.util.make_ssl_server_context(*certs)
    client_ctx = aiomas.util.make_ssl_client_context(*certs)

    await ctx.start_server(handle_client, ssl=server_ctx)
    channel = await ctx.connect(ssl=client_ctx)
    reply = await channel.send('ohai')
    assert reply == 'cya'
    await channel.close()


@pytest.mark.asyncio
async def test_ssl_error(ctx, certs):
    if type(ctx.addr) is not tuple:
        pytest.skip('Test SSL only with TCP sockets.')

    async def handle_client(channel):
        await channel.close()

    server_ctx = aiomas.util.make_ssl_server_context(*certs)
    client_ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_1)
    client_ctx.set_ciphers('ECDH+AESGCM')
    client_ctx.verify_mode = ssl.CERT_REQUIRED
    client_ctx.load_verify_locations(cafile=certs[0])
    client_ctx.check_hostname = True
    client_ctx.load_cert_chain(certfile=certs[1], keyfile=certs[2])

    await ctx.start_server(handle_client, ssl=server_ctx)
    with pytest.raises(ssl.SSLError):
        await ctx.connect(ssl=client_ctx)


@pytest.mark.asyncio
@pytest.mark.parametrize('timeout', [1, None])
async def test_connect_timeout(ctx, timeout):
    """Test that we don't get a timeout error if we start the server only a
    little late.

    Notes:

    - With a timeout "0", we always get an error if we start the server after
      the client – even if we don't sleep.

    - If we delay the server for only .5 seconds, a timeout of 1 and "None"
      (wait indefinitely) shoud suffice.

    """
    async def handle_client(channel):
        await channel.close()

    t_connect = ctx.loop.create_task(ctx.connect(timeout=timeout))
    await asyncio.sleep(.5)
    await ctx.start_server(handle_client)
    channel = await t_connect
    await channel.close()


@pytest.mark.asyncio
@pytest.mark.parametrize('timeout', [0, 1])
async def test_connect_timeout_error(ctx, timeout):
    """We should get an error if the delay between client and server start is
    to big."""
    async def handle_client(channel):
        await channel.close()

    async def client(addr, loop):
        channel = await aiomas.channel.open_connection(
            addr, loop=loop, timeout=timeout)
        await channel.close()

    t_connect = ctx.loop.create_task(ctx.connect(timeout=timeout))
    sleep = (timeout * 2 + 2) if ON_WINDOWS else (timeout * 2)
    await asyncio.sleep(sleep)
    await ctx.start_server(handle_client)
    with pytest.raises((ConnectionRefusedError, FileNotFoundError)):
        await t_connect
