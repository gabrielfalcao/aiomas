Authors
=======

The original author of aiomas is Stefan Scherfke.

The initial development has kindly been supported by `OFFIS
<www.offis.de/en/>`_.
